﻿/*
 * 程序名称: OAuth2U
 * 
 * 支持我们  http://donation.jumbotcms.net/
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

using System;
using OAuth2U.Common.Utils;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
namespace OAuth2U.WebFile.passport
{
    public partial class _register_third : OAuth2U.Common.UI.BasicPage
    {
        public string OAuthCode = "";
        public string OpenID = "";
        public string NickName = "";
        public string HeadUrl = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            string _sn = q("sn");
            string OAuth_Info = OAuth2U.Common.Utils.DES.DESDecrypt(_sn, StaticKey);
            try
            {
                JObject jsonObj = JObject.Parse(OAuth_Info);
                OAuthCode = jsonObj["oauthcode"].ToString();
                OpenID = jsonObj["openid"].ToString();
                NickName = jsonObj["nickname"].ToString();
                HeadUrl = jsonObj["headurl"].ToString();
            }
            catch (Exception ex)
            {
                CYQ.Data.Log.WriteLogToTxt(OAuth_Info);
            }
            //这里就是您自己的业务啦
        }
    }
}
